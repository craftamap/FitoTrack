<!--
  ~ Copyright (c) 2020 Jannis Scheibe <jannis@tadris.de>
  ~
  ~ This file is part of FitoTrack
  ~
  ~ FitoTrack is free software: you can redistribute it and/or modify
  ~     it under the terms of the GNU General Public License as published by
  ~     the Free Software Foundation, either version 3 of the License, or
  ~     (at your option) any later version.
  ~
  ~     FitoTrack is distributed in the hope that it will be useful,
  ~     but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~     GNU General Public License for more details.
  ~
  ~     You should have received a copy of the GNU General Public License
  ~     along with this program.  If not, see <http://www.gnu.org/licenses/>.
  -->

<resources>
    <string name="app_name">FitoTrack</string>
    <string name="workout_add">Add</string>
    <string name="stop">Stop</string>
    <string name="delete">Delete</string>
    <string name="exporting">Exporting</string>

    <string name="error">Error</string>
    <string name="errorGpxExportFailed">The GPX export has failed.</string>
    <string name="errorExportFailed">The data export has failed.</string>
    <string name="errorImportFailed">The data import has failed.</string>
    <string name="shareFile">Share file</string>
    <string name="initialising">Initialising</string>
    <string name="preferences">Preferences</string>
    <string name="workouts">Workouts</string>
    <string name="locationData">Location data</string>
    <string name="converting">Converting</string>
    <string name="finished">Finished</string>
    <string name="loadingFile">Loading file</string>
    <string name="chooseBackupFile">Choose Backup-File</string>

    <string name="importBackupMessage">WARNING: All your existing data in the app will be cleared. If you don\'t want to loose them, first take another backup. Are you sure, you want to restore this backup?</string>
    <string name="restore">Restore</string>
    <string name="backup">Backup</string>

    <string name="pref_ringtone_silent">Silent</string>

    <string name="setPreferencesTitle">Set Preferences</string>
    <string name="setPreferencesMessage">You can set your preferred unit system and your weight in the settings.</string>

    <string name="timeMinuteSingular">Minute</string>
    <string name="timeMinutePlural">Minutes</string>
    <string name="timeMinuteShort">min</string>
    <string name="timeHourSingular">Hour</string>
    <string name="timeHourPlural">Hours</string>
    <string name="timeHourShort">h</string>
    <string name="timeSecondsShort">s</string>
    <string name="and">and</string>

    <string name="errorEnterValidNumber">Enter a valid number</string>
    <string name="errorWorkoutAddFuture">The workout cannot be in the future</string>
    <string name="errorEnterValidDuration">Enter a valid duration</string>

    <string name="announcementGPSStatus">GPS Status</string>

    <string name="voiceAnnouncementsTitle">Voice Announcements</string>

    <string name="pref_voice_announcements_summary">Configure voice prompts that inform you during the workout</string>

    <string name="pref_announcements_config_title">Announcement Trigger</string>
    <string name="pref_announcements_config_summary">Choose the time and distance between voice announcements</string>

    <string name="pref_announcements_content">Content of the announcements</string>

    <string name="gpsLost">GPS signal lost</string>
    <string name="gpsFound">GPS signal found</string>

    <string name="workoutTime">Time</string>
    <string name="workoutDate">Date</string>
    <string name="workoutDuration">Duration</string>
    <string name="workoutPauseDuration">Pause Duration</string>
    <string name="workoutStartTime">Start Time</string>
    <string name="workoutEndTime">End Time</string>
    <string name="workoutDistance">Distance</string>
    <string name="workoutPace">Pace</string>
    <string name="workoutRoute">Route</string>
    <string name="workoutSpeed">Speed</string>
    <string name="workoutAvgSpeedShort">Avg. Speed</string>
    <string name="workoutAvgSpeedLong">Average Speed</string>
    <string name="workoutTopSpeed">Top Speed</string>
    <string name="workoutBurnedEnergy">Burned Energy</string>
    <string name="workoutTotalEnergy">Total Energy</string>
    <string name="workoutEnergyConsumption">Energy Consumption</string>
    <string name="workoutEdited">This workout has been edited.</string>
    <string name="uploading">Uploading</string>
    <string name="enterVerificationCode">Enter Verification Code</string>
    <string name="authenticationFailed">Authentication failed.</string>
    <string name="upload">Upload</string>
    <string name="uploadSuccessful">Upload Successful</string>
    <string name="uploadFailed">Upload Failed</string>
    <string name="uploadFailedOsmNotAuthorized">Not authorized, try again</string>

    <string name="workoutAscent">Ascent</string>
    <string name="workoutDescent">Descent</string>

    <string name="height">Height</string>


    <string name="workoutTypeRunning">Running</string>
    <string name="workoutTypeWalking">Walking</string>
    <string name="workoutTypeJogging">Jogging</string>
    <string name="workoutTypeCycling">Cycling</string>
    <string name="workoutTypeHiking">Hiking</string>
    <string name="workoutTypeOther">Other</string>
    <string name="workoutTypeUnknown">Unknown</string>
    <string name="type">Type</string>

    <string name="enterWorkout">Enter Workout</string>

    <string name="setDuration">Set Duration</string>


    <string name="recordWorkout">Record Workout</string>

    <string name="noGpsTitle">GPS disabled</string>
    <string name="noGpsMessage">Please enable GPS for tracking your workout.</string>
    <string name="enable">Enable</string>

    <string name="comment">Comment</string>
    <string name="enterComment">Enter Comment</string>
    <string name="okay">Okay</string>

    <string name="stopRecordingQuestion">Stop Recording?</string>
    <string name="stopRecordingQuestionMessage">Do you really want to stop the recording?</string>

    <!-- leave the underscore because "continue" is a java command -->
    <string name="continue_">Continue</string>

    <string name="deleteWorkout">Delete Workout</string>
    <string name="deleteWorkoutMessage">Do you really want to delete the workout?</string>

    <string name="trackerRunning">Tracker is running</string>
    <string name="trackerRunningMessage">Your workout is being recorded</string>

    <string name="trackingInfo">Tracking Info</string>
    <string name="trackingInfoDescription">Info about the tracker running</string>

    <string name="cancel">Cancel</string>
    <string name="exportAsGpxFile">Export as GPX-File</string>
    <string name="pref_weight">Your Weight</string>
    <string name="pref_weight_summary">Your weight is needed to calculate the burned calories</string>
    <string name="pref_unit_system">Preferred system of units</string>
    <string name="settings">Settings</string>
    <string name="exportData">Export Data</string>
    <string name="exportDataSummary">This takes a backup of all workouts</string>
    <string name="importBackup">Import Data Backup</string>
    <string name="importBackupSummary">Restore a taken backup</string>
    <string name="gps">GPS</string>
    <string name="data">Data</string>
    <string name="mapStyle">Map Style</string>
    <string name="waiting_gps">Waiting for GPS</string>
    <string name="actionUploadToOSM">Upload to OSM</string>
    <string name="cut">Cut the first/last 300 Meters</string>
    <string name="trackVisibilityPref">Track Visibility</string>
    <string name="description">Description</string>
    <string name="ttsNotAvailable">TextToSpeech is not available</string>
    <string name="action_edit_comment">Edit Comment</string>
    <string name="pref_announcement_mode">Announcement Mode</string>

    <string name="save">Save</string>
    <string name="share">Share</string>
    <string name="savedToDownloads">Saved to Downloads</string>
    <string name="savingFailed">Saving failed</string>
    <string name="info">Info</string>
    <string name="OpenStreetMapAttribution">© OpenStreetMap contributors</string>
    <string name="theme">Theme</string>
    <string name="hintRestart">Please restart the app to apply changes</string>
    <string name="noComment">No comment</string>
    <string name="start">Start</string>
    <string name="cannotStart">Cannot start</string>

    <string name="add">Add</string>
    <string name="add_interval">Add Interval</string>
    <string name="manageIntervalSets">Manage Interval Sets</string>
    <string name="manageIntervalsSummary">Edit the interval sets you will be able to use during the workout using voice feedback.</string>
    <string name="workoutRecording">Workout Recording</string>
    <string name="createIntervalSet">Create New Interval Set</string>
    <string name="create">Create</string>
    <string name="hintManageIntervalSets">Add an interval set to use it in your workouts.</string>
    <string name="hintEditIntervalSet">Add intervals that will be spoken.</string>
    <string name="interval_name">Name:</string>
    <string name="length_in_minutes">Duration (minutes):</string>
    <string name="enterName">Enter a name</string>
    <string name="deleteIntervalSet">Delete Interval Set</string>
    <string name="deleteIntervalSetMessage">Do you want to delete the interval set?</string>
    <string name="selectIntervalSet">Select Interval Set</string>
    <string name="intervalSetSelected">Interval Set selected</string>
    <string name="intervalsIncludePauseTitle">Include Pause Time in Interval Training</string>
    <string name="intervalsIncludePauseSummary">If true, the intervals will continue even if you do pauses in your workout.</string>
    <string name="intervalTraining">Interval Training</string>
    <string name="intervalSet">Interval Set</string>
    <string name="intervalSets">Interval Sets</string>
    <string name="currentSpeed">Current Speed</string>
    <string name="hintAddWorkout">Click on the plus icon to record or enter a workout.</string>
    <string name="enterDescription">Enter a description</string>
    <string name="avgSpeedInMotion">Avg. Speed (in Motion)</string>
    <string name="avgSpeedTotalShort">Avg. Speed (Total)</string>
    <string name="avgSpeedTotalLong">Average Speed in total</string>
    <string name="preferencesUserInterfaceTitle">User Interface</string>
    <string name="preferencesUserInterfaceSummary">Personalize the app</string>
    <string name="preferencesRecordingTitle">Recording</string>
    <string name="preferencesRecordingSummary">Manage the workout recording</string>
    <string name="preferencesBackupTitle">Database</string>
    <string name="preferencesBackupSummary">Backup/Restore your workout data</string>
    <string name="preferencesCategoryAppearance">Appearance</string>
    <string name="preferenceDateFormat">Date Format</string>
    <string name="preferenceTimeFormat">Time Format</string>
    <string name="workoutDiscarded">Workout discarded</string>
    <string name="timeSecondsSingular">Second</string>
    <string name="timeSecondsPlural">Seconds</string>
    <string name="per">per</string>
    <string name="unitMilesSingular">Mile</string>
    <string name="unitMilesPlural">Miles</string>
    <string name="unitYardsSingular">Yard</string>
    <string name="unitYardsPlural">Yards</string>
    <string name="unitMilesPerHour">Miles per Hour</string>
    <string name="unitMetersPlural">Meters</string>
    <string name="unitMetersSingular">Meter</string>
    <string name="unitKilometersPlural">Kilometers</string>
    <string name="unitKilometersSingular">Kilometer</string>
    <string name="unitKilometersPerHour">Kilometers per hour</string>
    <string name="unitMetersPerSecond">Meters per second</string>
    <string name="unitKcalLong">Calories</string>
    <string name="unitKJoule">Kilojoule</string>
    <string name="pref_energy_unit">Energy unit</string>
    <string name="workoutTypeInlineSkating">Inline Skating</string>


    <string-array name="pref_energy_units">
        <item>kcal</item>
        <item>Joule</item>
    </string-array>

    <string-array name="pref_announcement_mode">
        <item>Always</item>
        <item>Only on headphones</item>
    </string-array>

    <string-array name="pref_map_layers">
        <item>Mapnik\nDefault OSM Mapstyle</item>
        <item>Humanitarian\nClean map but sometimes not available</item>
        <item>Outdoors\nLimited access</item>
        <item>Cyclemap\nLimited access</item>
    </string-array>

    <string-array name="pref_theme_setting">
        <item>Light</item>
        <item>Dark</item>
    </string-array>

    <string-array name="pref_unit_systems">
        <item>Metric</item>
        <item>Metric with m/s</item>
        <item>Imperial</item>
        <item>Imperial with meters</item>
    </string-array>

    <string-array name="pref_time_format">
        <item>System Default</item>
        <item>12:08 PM</item>
        <item>15:23</item>
    </string-array>

    <string-array name="pref_date_format">
        <item>System Default</item>
        <item>2001-07-24</item>
        <item>2001.07.24</item>
        <item>2001/07/24</item>
        <item>24-07-2001</item>
        <item>24.07.2001</item>
        <item>24/07/2001</item>
        <item>07/24/2001</item>
    </string-array>

</resources>
